import random

from src.add_bass import add_bass
from src.config.config import Config, load_config
from src.export_midi import export_midi
from src.finish_canon import finish_canon
from src.harmonize import harmonize
from src.model.scale import ScaleEnum, randomize_root
from src.polish_canon import polish_canon
from src.scale_provider import init_scale_provider
from src.start_canon import start_canon


def main():
    load_config()

    if Config().seed:
        random.seed(Config().seed)

    init_scale_provider(randomize_root(), ScaleEnum.MAJOR)

    canon = start_canon()

    for _ in range(1, Config().n_bars - 3):
        canon.add_bar(harmonize(canon.second_voice.bars[-1]))

    finish_canon(canon)

    if Config().polish_canon:
        polish_canon(canon)

    if Config().add_bass:
        add_bass(canon)

    export_midi(canon)


if __name__ == "__main__":
    main()
