import logging

from src.model.midi_pitches import MidiPitch
from src.model.scale import Scale, ScaleEnum
from src.utils.singleton import Singleton


class ScaleProvider(metaclass=Singleton):
    # pylint: disable=too-few-public-methods
    def __init__(
        self,
        scale: Scale = None,
    ):
        if scale is None:
            logging.error("failed to initialize %s", ScaleProvider.__name__)
            return

        self.scale = scale

    def set_scale(self, scale: Scale):
        self.scale = scale


def init_scale_provider(root: MidiPitch, scale_enum: ScaleEnum):
    ScaleProvider(Scale(root, scale_enum))
