import random

from typing import List

from src.model.canon import Canon
from src.model.intervals import Interval, get_interval
from src.model.midi_pitches import MidiPitch
from src.model.note_values import NoteValue
from src.model.voice import Voice
from src.utils.bar_utils import hold_for_duration, create_pause
from src.utils.pitch_utils import extract_downbeat_pitches
from src.utils.transpose import transpose_up, transpose_down
from src.scale_provider import ScaleProvider


def add_bass(canon: Canon):
    third_voice = Voice()
    third_voice.add_bar(create_pause(NoteValue.HALF))

    # pretend bass starts on first note of second voice transposed down an
    # octave
    bass = transpose_down(
        canon.second_voice.bars[1].notes[0].pitch,
        Interval.OCTAVE,
    )
    for i in range(1, canon.n_bars() - 2):
        first_voice_downbeats = extract_downbeat_pitches(
            canon.first_voice.bars[i].notes)
        second_voice_downbeats = extract_downbeat_pitches(
            canon.second_voice.bars[i].notes)
        bass = random.choice(
            find_bass_pitches(
                first_voice_downbeats,
                second_voice_downbeats,
                bass,
            ))
        third_voice.add_bar(hold_for_duration(bass, NoteValue.HALF))

    tonic = transpose_down(
        canon.second_voice.bars[-1].notes[-1].pitch,
        Interval.OCTAVE,
    )
    dominant = find_closest_dominant(
        tonic,
        third_voice.bars[-1].notes[-1].pitch,
    )
    third_voice.add_bar(hold_for_duration(dominant, NoteValue.HALF))
    third_voice.add_bar(hold_for_duration(tonic, NoteValue.HALF))
    canon.third_voice = third_voice


def find_bass_pitches(
    first_voice_downbeats: List[MidiPitch],
    second_voice_downbeats: List[MidiPitch],
    prev_pitch: MidiPitch,
) -> List[MidiPitch]:
    candidates = [
        pitch for pitch in ScaleProvider().scale.pitches if is_bass_candidate(
            pitch,
            first_voice_downbeats[0],
            second_voice_downbeats[0],
            prev_pitch,
        )
    ]

    if len(candidates) != 0:
        return candidates

    return [transpose_down(second_voice_downbeats[0], Interval.OCTAVE)]


def is_bass_candidate(
    pitch: MidiPitch,
    first_voice_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
    prev_pitch: MidiPitch,
) -> bool:
    # pylint: disable=too-many-return-statements
    if pitch.value < second_voice_pitch.value - Interval.OCTAVE.value - Interval.FIFTH.value:
        return False

    if pitch.value > second_voice_pitch.value - Interval.FIFTH.value:
        return False

    if abs(pitch.value - prev_pitch.value) > Interval.OCTAVE.value:
        return False

    if abs(pitch.value - ScaleProvider().scale.root.value) > 3 * Interval.OCTAVE.value:
        return False

    if not get_interval(pitch, first_voice_pitch).is_chord_tone():
        return False

    if not get_interval(pitch, second_voice_pitch).is_chord_tone():
        return False

    return True


def find_closest_dominant(
    tonic: MidiPitch,
    prev_pitch: MidiPitch,
) -> MidiPitch:
    up_a_fifth = transpose_up(tonic, Interval.FIFTH)
    down_a_fourth = transpose_down(tonic, Interval.FOURTH)
    if (abs(up_a_fifth.value - prev_pitch.value) <
            abs(down_a_fourth.value - prev_pitch.value)):
        return up_a_fifth

    return down_a_fourth
