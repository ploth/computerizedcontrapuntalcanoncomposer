import random

from typing import List, Tuple

from src.model.canon import Canon
from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch
from src.model.note import Note
from src.model.note_values import NoteValue
from src.utils.transpose import transpose_down
from src.utils.pitch_utils import find_transitional_pitches


def polish_canon(canon: Canon):
    first_voice_notes = canon.first_voice.flat()
    second_voice_notes = canon.second_voice.flat()
    i_i, t_1, i_2, t_2 = 0, 0.0, 0, 0.0
    duration = canon.n_bars() * NoteValue.HALF.value
    duration_minus_final_bars = duration - 2 * NoteValue.HALF.value
    n_steps = int(duration_minus_final_bars / NoteValue.EIGTH.value)
    for step in range(n_steps):
        time = step * NoteValue.EIGTH.value
        n_1, i_i, t_1 = next_step(first_voice_notes, i_i, t_1, time)
        _, i_2, t_2 = next_step(second_voice_notes, i_2, t_2, time)

        if time % NoteValue.QUARTER.value == 0:
            # not on offbeat
            continue

        if n_1.value != NoteValue.EIGTH:
            continue

        new_pitch = find_transition(
            first_voice_notes[i_i - 1].pitch,
            first_voice_notes[i_i + 1].pitch,
            second_voice_notes[i_2].pitch,
        )
        first_voice_notes[i_i].pitch = new_pitch
        second_voice_notes[i_i + 1].pitch = transpose_down(
            new_pitch, Interval.OCTAVE)


def next_step(
    notes: List[Note],
    i: int,
    prev_time: float,
    time: float,
) -> Tuple[Note, int, float]:
    note = notes[i]
    if time >= prev_time + note.value.value:
        return (notes[i + 1], i + 1, time)
    return (note, i, prev_time)


def find_transition(
    prev_pitch: MidiPitch,
    next_pitch: MidiPitch,
    second_voice_pitch: MidiPitch,
) -> MidiPitch:
    return random.choice(
        find_transitional_pitches(
            prev_pitch,
            next_pitch,
            second_voice_pitch,
        ))
