from typing import Dict

import yaml

from src.utils.singleton import Singleton


class Config(metaclass=Singleton):
    # pylint: disable=too-few-public-methods
    def __init__(self, config: Dict = None):
        if config is not None:
            self.seed = config["seed"]
            self.n_bars = config["n_bars"]
            self.polish_canon = config["polish_canon"]
            self.fix_rythm = config["fix_rythm"]
            self.add_bass = config["add_bass"]


def load_config(path_to_config="config.yaml"):
    with open(path_to_config, "r") as file:
        config: Dict = yaml.safe_load(file)
        Config(config)
