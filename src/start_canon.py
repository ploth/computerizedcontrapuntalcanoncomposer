import random

from typing import List

from src.model.canon import Canon
from src.model.intervals import Interval
from src.model.midi_pitches import MidiPitch
from src.scale_provider import ScaleProvider
from src.utils.bar_utils import create_bar_from_rythm_and_downbeat_pitches
from src.utils.rythm_utils import create_exciting_rythm


def start_canon() -> Canon:
    return Canon(
        create_bar_from_rythm_and_downbeat_pitches(
            create_exciting_rythm(),
            create_initial_downbeats(),
        ))


def create_initial_downbeats() -> List[MidiPitch]:
    first_pitch = ScaleProvider().scale.root
    return [
        first_pitch,
        random.choice(find_second_pitch_candidates(first_pitch)),
    ]


def find_second_pitch_candidates(first_pitch: MidiPitch) -> List[MidiPitch]:
    return [
        pitch for pitch in ScaleProvider().scale.upper_register
        if pitch != first_pitch
        and abs(pitch.value - first_pitch.value) <= Interval.FIFTH.value
    ]
