from typing import List

from src.model.note import Note
from src.model.rythm import Rythm


class Bar:
    def __init__(self, notes: List[Note]):
        self.notes = notes
        self.rythm = Rythm([note.value for note in notes])

    def __str__(self):
        return "Bar: [" + ", ".join([str(note) for note in self.notes]) + "]"

    def __repr__(self):
        return self.__str__()
