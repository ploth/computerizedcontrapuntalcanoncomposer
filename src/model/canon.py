import logging

from src.model.bar import Bar
from src.model.intervals import Interval
from src.model.note import Note
from src.model.note_values import NoteValue
from src.model.voice import Voice
from src.utils.bar_utils import create_pause
from src.utils.transpose import transpose_down


class Canon():
    def __init__(self, initial_bar: Bar):
        self.first_voice = Voice()
        self.second_voice = Voice()
        self.second_voice.add_bar(create_pause(NoteValue.HALF))
        self.add_bar(initial_bar)
        self.third_voice = Voice() # bass

    def __str__(self):
        ret = ""
        for first, second in zip(self.first_voice.bars, self.second_voice.bars):
            ret += "first_voice\n"
            ret += str(first)
            ret += "\n"
            ret += "second_voice\n"
            ret += str(second)
        return ret

    def __repr__(self):
        return self.__str__()

    def add_bar(self, bar: Bar):
        if not bar.rythm.check_length():
            logging.error("%s does not have the correct length", bar)
        self.first_voice.add_bar(bar)
        self.copy_to_second_voice()

    def copy_to_second_voice(self):
        self.second_voice.add_bar(
            Bar([
                Note(note.value, transpose_down(note.pitch, Interval.OCTAVE))
                for note in self.first_voice.bars[-1].notes
            ]))

    def n_bars(self) -> int:
        # second voice has correct number of bars, first voice is one bar short
        # because the final bar is a whole note, which spans two bars
        return len(self.second_voice.bars)

    def n_voices(self) -> int:
        return 2 if self.third_voice.is_empty() else 3
