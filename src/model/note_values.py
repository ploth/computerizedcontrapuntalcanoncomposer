from enum import Enum


class NoteValue(Enum):
    WHOLE = 1
    HALF = 1 / 2
    QUARTER = 1 / 4
    DOTTED_QUARTER = 3 / 8
    EIGTH = 1 / 8

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return self.__str__()

    def to_midi(self):
        # a quarter note has a value of 1 in midi
        return self.value * 4
